**Provide build triggers/approaches for**

1. Pull request with basic testing (PR pipeline)
2. Branch updates with different tests and stages (Branch_update pipeline)
3. Tag push for CD (Tag pipeline)
4. "Promote" to master branch (Periodic Pipeline)
